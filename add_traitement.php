<?php
require_once 'database.php';

if(!empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['phone']) && !empty($_POST['email'])){
    $firstname = htmlspecialchars($_POST['firstname']);
    $lastname = htmlspecialchars($_POST['lastname']);
    $phone = htmlspecialchars($_POST['phone']);
    $email = htmlspecialchars($_POST['email']);

    $check = $bdd->prepare('INSERT INTO contact(firstname,lastname,phone,email) VALUES (:firstname,:lastname,:phone,:email)');
    $check->execute(array(
        'firstname' => $firstname,
        'lastname' => $lastname ,
        'phone' => $phone,
        'email' => $email,
    ));
    header('Location:index.php?add_err=success');
}else header('Location:index.php?add_err=here');