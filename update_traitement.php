<?php
require_once 'database.php';
if(!empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['phone']) && !empty($_POST['email']) && !empty($_POST['id'])) {
    $id =htmlspecialchars($_POST['id']);
    $firstname =htmlspecialchars($_POST['firstname']);
    $lastname =htmlspecialchars($_POST['lastname']);
    $phone =htmlspecialchars($_POST['phone']);
    $email =htmlspecialchars($_POST['email']);

    $update= $bdd->prepare("UPDATE contact SET firstName = :firstname, lastName = :lastname, phone = :phone, email = :email WHERE id = :id");
    $update->execute(array(
       'firstname' => $firstname,
        'lastname' => $lastname,
        'phone' => $phone,
        'email' => $email,
        'id' => $id
    ));
    header('Location:index.php?upd_upt=nice');
}